<body id="page-top">
    <?php 
    $hostname = 'localhost';
    $database = 'registropr2';
    $username = 'root';
    $password = '';
    $tbl_name = "recetas";
    
    
    $mysqli = new mysqli($hostname,$username,$password,$database);
    

    // Establecemos el numero de pagina en la que el usuario se encuentra.
# Esto lo hacemos por el metodo GET, si no hay ningun valor entonces le asignamos la pagina 1.
$pagina = isset($_GET['pagina']) ? (int)$_GET['pagina'] : 1;

// Establecemos cuantos post por pagina queremos cargar.
$postPorPagina = 4;

// Revisamos desde que usuario vamos a cargar, dependiendo de la pagina en la que se encuentre el usuario.
# Comprobamos si la pagina en la que esta es mayor a 1, sino entonces cargamos desde el usuario 0.
# Si la pagina es mayor a 1 entonces hacemos un calculo para saber desde que post cargaremos.
$inicio = ($pagina > 1) ? ($pagina * $postPorPagina - $postPorPagina) : 0 ;

// Preparamos y ejecutamos la consulta SQL
//Se obtienen los usuarios
$query = "SELECT * FROM $tbl_name";
$result = $mysqli->query($query);

// Calculamos el total de usuarios, para despues conocer el numero de paginas de la paginacion.
$totalArticulos = $result->num_rows;	

if ($totalArticulos < 1){
	header('Location: http://www.google.es/');
}

//Se obtienen los articulos para la página seleccionada
$query = "SELECT SQL_CALC_FOUND_ROWS * FROM $tbl_name
	        LIMIT $inicio, $postPorPagina";
$result = $mysqli->query($query);


// Calculamos el numero de paginas que tendra la paginacion.
# Para esto dividimos el total de usuarios entre los post por pagina
$numeroPaginas = ceil($totalArticulos / $postPorPagina);

//HASTA AQUI ES PAGINACION





require 'index.view.php';
    ?>
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark navbar-custom fixed-top">
            <div class="container px-5">
                <a class="navbar-brand" href="formulario/indexFrom.php">Registrasre</a>
                <a class="navbar-brand" href="login/indexLogin.php">Iniciar sesion</a>
                <a class="navbar-brand" href="perfil/perfil.php">Perfil</a>
                <a class="navbar-brand" href="perfil/destroy.php">Cerrar sesion</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <!-- <ul class="navbar-nav ms-auto">
                              
                    </ul> -->
                </div>
            </div>
        </nav>
        
        <!-- Lista de recetas -->        
        
        <!-- Footer-->
        <footer class="py-5 bg-black">
            <div class="container px-5"><p class="m-0 text-center text-white small">Copyright &copy; Your Website 2021</p></div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
    </body>