<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="css2.css">
	<title>Paginacion</title>
</head>
<body>
    <?php 
    $hostname = 'localhost';
    $database = 'registropr2';
    $username = 'root';
    $password = '';
    $tbl_name = "recetas";
    
    
    $mysqli = new mysqli($hostname,$username,$password,$database);
	$sql = "SELECT * FROM recetas";
	$result = mysqli_query($mysqli,$sql);

    ?>
	<div class="contenedor">
		<h1>Recetas</h1>
        <?php 
		while ($mostrar  = mysqli_fetch_array($result)){ ?>
			<section class="articulos">
			<ul>
            <div class="card-body">
				<ul class="list-group list-group-flush">
					<li><?php echo $mostrar['nombre_receta']?></li>
				</ul>
				<div class="cat">
				<?php echo '<img src="assets/img/'.$mostrar['foto'].'.jpg"/>'?>
		</div>
			</div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><?php echo $mostrar['ingredientes']?></li>
                <li class="list-group-item"><?php echo $mostrar['elaboracion']?></li>
            </ul>
		</section>
		<?php } ?>



			<!-- PAGINACION -->
		<div class="paginacion">
			<ul>
				<!-- Establecemos cuando el boton de "Anterior" estara desabilitado -->
				<?php if($pagina == 1): ?>
					<li class="disabled">&laquo;</li>
				<?php else: ?>
					<li><a href="?pagina=<?php echo $pagina - 1 ?>">&laquo;</a></li>
				<?php endif; ?>

				<!-- Ejecutamos un ciclo para mostrar las paginas -->
				<?php 
					for($i = 1; $i <= $numeroPaginas; $i++){
						if ($pagina === $i) {
							echo "<li class='active'><a href='?pagina=$i'>$i</a></li>";
						} else {
							echo "<li><a href='?pagina=$i'>$i</a></li>";
						}
					}
				 ?>

				<!-- Establecemos cuando el boton de "Siguiente" estara desabilitado -->
				<?php if($pagina == $numeroPaginas): ?>
					<li class="disabled">&raquo;</li>
				<?php else: ?>
					<li><a href="?pagina=<?php echo $pagina + 1 ?>">&raquo;</a></li>
				<?php endif; ?>
					
			</ul>
		</div>
	</div>
</body>
</html>