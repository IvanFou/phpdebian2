-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-02-2022 a las 20:36:25
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `registropr2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `id` int(11) NOT NULL,
  `nick` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `contrasena` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `apellido_1` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `apellido_2` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `edad` int(11) NOT NULL,
  `telefono` int(20) NOT NULL,
  `email` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`id`, `nick`, `contrasena`, `nombre`, `apellido_1`, `apellido_2`, `edad`, `telefono`, `email`) VALUES
(1, 'admin1', '1234', 'Dios', 'Dios', 'Dios', 100, 2032320, 'dios@dios.com'),
(2, 'admin2', '1234', 'Patata', 'Patatoide', 'Juanpedro', 456, 547474, 'patata@patata.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recetas`
--

CREATE TABLE `recetas` (
  `id` int(11) NOT NULL,
  `nombre_receta` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `foto` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `ingredientes` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `elaboracion` varchar(250) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `recetas`
--

INSERT INTO `recetas` (`id`, `nombre_receta`, `foto`, `ingredientes`, `elaboracion`) VALUES
(2, 'Sopa', 'sopa', 'Caldo', 'Calentar caldo'),
(3, 'Canelones', 'canelones', 'Carne picada, cebolla, pimiento, salsa de tomate', 'Cocinarlos'),
(4, 'Lentejas', 'lentejas', 'Lentejas, pimiento verde, cebolla, puerro, zanahoria, patata, salsa de tomate, aceite de oliva, diente de ajo, laurel y sal', 'Picamos en trozos menudos el ajo, la cebolla, la zanahoria, el pimiento verde y el puerro. Calentamos un poco de aceite de oliva en una cacerola, aÃ±adimos las verduras y sofreÃ­mos a fuego bajo durante cinco minutos. Sazonamos ligeramente, agregamos'),
(5, 'Pizza', 'pizza', 'Harina de trigo, agua templada, sal fina, aceite de oliva, levadura seca de panaderia o fresca', 'En un vaso, vierte el agua templada. Disuelve la levadura en el agua. En un bol ponemos la harina, la sal y, poco a poco, vertemos el agua con la levadura. Es mÃ¡s fÃ¡cil si vamos amasando poco a poco a medida que echamos el agua, hasta verter toda y'),
(6, 'Pollo con miel', 'pollo', 'Pollo, sal, agua, especias como laurel, tomillo, miel.', 'Deshuesar el pollo, hervirlo en agua con sal, echarle especias, y dejar que se cocine.'),
(7, 'Paella de marisco', 'paella', 'Gambas, arroz, mejillones, sepia, almejas, azafran/colorante, pimiento, cebolla, dientes de ajo, tomate', 'Empezamos preparando las verduras para el sofrito. Lava los pimientos y quÃ­tales el pedÃºnculo, pela la cebolla y los ajos, y pÃ­calo todo muy finito. Para ello, hemos utilizado nuestra picadora manual con la que conseguimos tenerlo todo picadito en'),
(8, 'Gambas, gambones y langostinos', 'gambones', 'Gambones o langostinos grandes, diente de ajo pelado, aceite de oliva virgen extra, sal gruesa, vino blanco o cava', 'Comenzamos precalentando el horno a 190ÂºC con calor arriba y abajo y retiramos los bigotes de los gambones o langostinos. Los colocamos en una fuente apta para el horno, preferiblemente de cerÃ¡mica para se cocinen a la vez por arriba y por abajo.  ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nick` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `contrasena` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `apellido_1` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `apellido_2` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `edad` int(11) NOT NULL,
  `telefono` int(20) NOT NULL,
  `email` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nick`, `contrasena`, `nombre`, `apellido_1`, `apellido_2`, `edad`, `telefono`, `email`) VALUES
(7, 'Yellow', '1234', 'Ivan', 'Calvo', 'Burguete', 27, 682037727, 'SuperIvan@ivan.com'),
(8, 'Pietro', '1234', 'Pedro', 'Buil', 'GrazieMille', 21, 2304934, 'pietro@pietro.com'),
(9, 'Nere', '1234', 'Nerea', 'Canas', 'Blazquez', 21, 32430403, 'nerea@nerea.com'),
(11, 'Teleko', '1234', 'Dani', 'Cardenas', 'Teleco', 21, 666666666, 'dani@teleko.com'),
(12, 'Prueba', '1234', 'apsd', 'asÃ±d', 'asd', 23, 13239, 'sajdl'),
(13, 'Patata', '1234', 'Patataoide', 'Tuberculo', 'Juanpedro', 456, 547474, 'patata@patata.com'),
(14, 'Vicente', '1234', 'VicenteCambiado', 'Vicente1', 'Vicente2', 21, 666666666, 'vicente@vicente.com');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `recetas`
--
ALTER TABLE `recetas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `recetas`
--
ALTER TABLE `recetas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
